package com.example.springboot.config;

import com.example.springboot.pojo.User;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Created by bothin
 * At 2020/4/21 18:37
 * Description:
 */
@Configuration
public class BeanConfig {

    @Bean
    public User getUser(){
        return new User("bothin",23);
    }

}
