package com.example.springboot.config;

import com.example.springboot.pojo.User;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 * Created by bothin
 * At 2020/4/21 20:42
 * Description:
 */
@Configuration
@Import(BeanConfig.class)
public class TestImportConfig {

    @Bean
    public User testGetUserByImport(User user){
        System.out.println(user);
        return user;
    }
}
