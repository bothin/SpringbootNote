package com.example.springboot.config;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * Created by bothin
 * At 2020/4/21 17:44
 * Description:
 */
@SpringBootTest
class PersonConfigTest {

    @Autowired
    private PersonConfig personConfig;

    @Test
    public void test01(){
        System.out.println(personConfig);
    }
}