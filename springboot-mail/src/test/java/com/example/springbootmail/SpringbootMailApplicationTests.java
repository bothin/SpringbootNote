package com.example.springbootmail;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.File;

@SpringBootTest
class SpringbootMailApplicationTests {
    @Autowired
    JavaMailSenderImpl mailSender;

    @Test
    void contextLoads() {
        //一个简单邮件 （文字）
        SimpleMailMessage mailMessage = new SimpleMailMessage();

        mailMessage.setSubject("使用spring boot发送邮件");
        mailMessage.setText("我发送的第一封spring boot邮件");
        mailMessage.setTo("1824751554@qq.com"); //接收人
        mailMessage.setFrom("1824751554@qq.com");   //发送人

        mailSender.send(mailMessage);
    }
    
    @Test
    public void testSendMimeMessage() throws MessagingException {
        //一个复杂邮件
        MimeMessage mimeMessage = mailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true);


        helper.setSubject("使用spring boot发送邮件");
        helper.setText("我发送的第一封spring boot邮件");

        //附件
        helper.addAttachment("application.properties",new File("D:\\Java\\Codes\\SpringBootNote\\springboot-mail\\src\\main\\resources\\application.properties"));

        helper.setTo("1824751554@qq.com"); //接收人
        helper.setFrom("1824751554@qq.com");   //发送人

        mailSender.send(mimeMessage);


    }

}
