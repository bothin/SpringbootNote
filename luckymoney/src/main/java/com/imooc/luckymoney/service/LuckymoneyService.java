package com.imooc.luckymoney.service;

import com.imooc.luckymoney.dao.LuckymoneyRepository;
import com.imooc.luckymoney.model.Luckymoney;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;

@Service
public class LuckymoneyService {

    @Autowired
    private LuckymoneyRepository repository;

    /**
     * 配置事务 @Transactional
     * 用jpa创建的表的默认引擎是myisqm,这个引擎不支持事务的，要改为InnoDB
     *
     */
    @Transactional
    public void createTwo(){
        Luckymoney luckymoney1 = new Luckymoney();
        luckymoney1.setProducer("bothin");
        luckymoney1.setMoney(new BigDecimal("520"));
        repository.save(luckymoney1);

        Luckymoney luckymoney2 = new Luckymoney();
        luckymoney1.setProducer("bothin");
        luckymoney1.setMoney(new BigDecimal("1314"));
        repository.save(luckymoney2);
    }
}
