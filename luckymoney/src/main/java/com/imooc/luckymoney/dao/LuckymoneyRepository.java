package com.imooc.luckymoney.dao;

import com.imooc.luckymoney.model.Luckymoney;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LuckymoneyRepository extends JpaRepository<Luckymoney,Integer> {
}
