package com.imooc.luckymoney.controller;


import com.imooc.luckymoney.config.LimitConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;

@RestController
public class HelloController {

    //从application.yml配置文件中引入信息
    @Value("${minmoney}")
    private BigDecimal minMoney;

    @Value("${description}")
    private String description;

    //通过LimitConfig注入信息
    @Autowired
    private LimitConfig limitConfig;

    //配置多路径访问 数组 @GetMapping({"/hello","/hi"})
    @GetMapping({"/hello","/hi"})
    public String say(){
        return "hello world";
    }

    @GetMapping("/hello1")
    public String say1(){
        return "通过@Value()读取yml中的配置信息" + " minmoney: "+minMoney + " description:"+description;
    }

    @GetMapping("/hello2")
    public String say2(){
        return "通过LimitConfig注入读取yml中的配置信息" + " minmoney: "+limitConfig.getMinMoney() +
                " maxmoney: "+limitConfig.getMaxMoney() + " description:"+limitConfig.getDescription();
    }

    //通过post请求访问
    @PostMapping("/post")
    public String post(){
        return "post请求：@PostMapping(\"/post\")";
    }

    //通过post/get请求访问（两种都行）
    @RequestMapping("/twoRequest")
    public String twoRequest(){
        return "通过post/get请求访问（两种都行）";
    }

    //访问路径 /getid/100
    @GetMapping("/getid/{id}")
    public String getPathVariable(@PathVariable("id") Integer id){
        return "id:"+id;
    }

    //访问路径 /getid?id=100
    @GetMapping("/getid2")
    public String getPathVariable2(@RequestParam(value = "id",required = false,defaultValue = "0") Integer id){
        return "id:"+id;
    }



}
