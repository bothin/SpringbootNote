package com.imooc.luckymoney.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Scope;

import java.util.Calendar;

/**
 * Created by bothin
 * At 2020/4/6 11:18
 * Description: 介绍一下spring boot创建的bean
 */

@Configuration
public class BeanConfig {

    @Bean//(name = "aaa")   //将方法的返回值交由spring boot管理，在工厂中name的默认标识是：类名首字母小写
    @Scope("prototype")     //spring boot创建的bean默认是单例 singleton，可以配置多例 prototype
    @Lazy                   //指定获取对象时再进行bean实例的创建，而不是在容器启动时创建bean实例
    public Calendar getCalendar(){
        return Calendar.getInstance();
    }
}
