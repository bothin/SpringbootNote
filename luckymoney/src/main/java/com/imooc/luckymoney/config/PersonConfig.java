package com.imooc.luckymoney.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * Created by bothin
 * At 2020/4/21 17:19
 * Description:
 */
@Component
@ConfigurationProperties(prefix = "person")
@Data
public class PersonConfig {
    private String name;
    private int age;
}
