package com.imooc.luckymoney;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 讲解：@SpringBootApplication注解主要是由@EnableAutoConfiguration和@ComponentScan组合成，
 *      根据引入的依赖进行自动配置应用，并扫描主类所在的包和子包中的组件进行配置
 */

@SpringBootApplication
public class LuckymoneyApplication {

    public static void main(String[] args) {
        SpringApplication.run(LuckymoneyApplication.class, args);
    }

}
