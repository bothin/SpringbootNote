package com.example.springbootjsp.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by bothin
 * At 2020/4/6 13:25
 * Description: 测试访问jsp页面
 */
@Controller
public class TestController {

    @RequestMapping("/index")
    public String getIndexJsp(){
        return "index";
    }
}
