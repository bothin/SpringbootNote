package com.example.springbootswagger.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.core.env.Profiles;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.ArrayList;

/**
 * Created by bothin
 * At 2020/4/26 21:23
 * Description:
 */
@Configuration
@EnableSwagger2
public class SwaggerConfig {

    @Bean
    public Docket docket(Environment environment){

        //配置需要启动swagger的环境，通常开发环境需要开启
        Profiles profiles = Profiles.of("dev","test");
        boolean flag = environment.acceptsProfiles(profiles);


        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
                .enable(flag)  //配置是否启动swagger
                .select()
                // RequestHandlerSelectors.RequestHandlerSelectors,配要扫接口的方式
                // RequestHandlerSelectors.basePackage:指定要扫描的包
                // RequestHandlerSelectors.any():扫描全部
                // RequestHandlerSelectors.none():不扫描
                // RequestHandlerSelectors.withClassAnnotation:扫描类上的注解(具有某个注解),参数是一个注解的反射对象
                // RequestHandlerSelectors.withMethodAnnotation:扫描方法上的注解
                .apis(RequestHandlerSelectors.basePackage("com.example.springbootswagger.controller"))
                // 配置扫描路径
                .paths(PathSelectors.ant("/**"))
                .build();
    }

    /**
     * 配置API文档的基本信息
     * @return ApiInfo
     */
    private ApiInfo apiInfo() {

        Contact contact = new Contact("bothin", "http://bothin.com", "1824751554@qq.com");

        return new ApiInfo(
                "Bothin's Api Documentation",
                "只要学不死，就要往死里学",
                "v1.0",
                "urn:tos",  //个人网站
                contact, //个人信息
                "Apache 2.0",
                "http://www.apache.org/licenses/LICENSE-2.0",
                new ArrayList());

    }


    /**
     * 配置多个分组
     * @return
     */
    @Bean
    public Docket docketA(){
        return new Docket(DocumentationType.SWAGGER_2).groupName("A");
    }

    /**
     * 配置多个分组
     * @return
     */
    @Bean
    public Docket docketB(){
        return new Docket(DocumentationType.SWAGGER_2).groupName("B");
    }

}
