package com.bothin.annotation;

import java.lang.annotation.*;

/**
 * Created by bothin
 * At 2020/4/10 18:23
 * Description: 自定义注解 可以参考我的文档 https://docs.qq.com/doc/DUktOdVRaQXJzcnRD
 */
public class TestMyAnnotation {


    @Documented //将注解中的元素包含到 Javadoc 中去
    @Retention(RetentionPolicy.RUNTIME) //说明了这个注解的的存活时间
    @Inherited //它所标注的Annotation将具有继承性。
    @Target({ElementType.FIELD,ElementType.METHOD}) //作用的目标
    public @interface myAnnotation{
        String[] value();
        String name() default "bothin";
        int age() default 10;
    }

}
