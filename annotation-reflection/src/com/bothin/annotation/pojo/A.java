package com.bothin.annotation.pojo;

public class A {
    static {
        System.out.println("A类的静态代码块初始化");
        m = 300;
    }

    public static int m = 100;

    public A() {
        System.out.println("A类的无参构造初始化");
    }
}