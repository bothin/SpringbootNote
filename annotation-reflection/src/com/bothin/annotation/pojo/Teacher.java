package com.bothin.annotation.pojo;

/**
 * Created by bothin
 * At 2020/4/12 18:10
 * Description:
 */
public class Teacher extends Person {
    private int money;

    public Teacher( int age,int money) {
        super("老师", age);
        this.money = money;
    }

    public Teacher() {
    }
}
