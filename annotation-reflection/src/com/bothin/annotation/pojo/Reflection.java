package com.bothin.annotation.pojo;

import java.io.Serializable;

/**
 * Created by bothin
 * At 2020/4/12 21:15
 * Description:
 */
public class Reflection extends Object implements Serializable {

    private int money;

    public int age;

    public String name;

    public Reflection(int money, int age, String name) {
        this.money = money;
        this.age = age;
        this.name = name;
    }

    public Reflection() {
    }

    public int getMoney() {
        return money;
    }

    public void setMoney(int money) {
        this.money = money;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    class TempA{
        int a;
    }

    class TempB{
        int b;
    }
}
