package com.bothin.annotation.pojo;

/**
 * Created by bothin
 * At 2020/4/12 18:09
 * Description:
 */
public class Student extends Person {
    public Student( int age) {
        super("学生", age);
    }

    public Student() {
    }
}
