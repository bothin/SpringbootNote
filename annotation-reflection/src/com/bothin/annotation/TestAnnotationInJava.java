package com.bothin.annotation;

/**
 * Created by bothin
 * At 2020/4/10 18:11
 * Description: 介绍一下内置注解 @FunctionalInterface @SuppressWarnings @Deprecated @Override
 */
public class TestAnnotationInJava {


    //阻止警告的意思
    @SuppressWarnings("all")
    public class Person extends Object{

        /*
        这个元素是用来标记过时的元素，编译器在编译阶段遇到这个注解时会发出提醒警告，告诉开发者正在调用
        一个过时的元素比如过时的方法、过时的类、过时的成员变量。
         */
        @Deprecated
        public void say(){
            System.out.println("Noting has to say!");
        }

        //提示子类要复写父类中被 @Override 修饰的方法
        @Override
        public String toString() {
            return super.toString();
        }
    }
}
